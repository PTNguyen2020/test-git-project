// tao 1 seed job se tao ra n job moi voi moi job nhan 1 parameter tu list_of_parameters hoac ta co the them moi thu cong

def number_of_jobs = 5
def list_of_parameters = ['A', 'B', 'C', 'D', 'E']
def url_of_git = 'https://gitlab.com/PTNguyen2020/test-git-project.git'
def credential = 'b4ee84cf-8473-447a-b948-441954a38201'
def branch_of_git = 'main'
def path = 'jenkinsfile-echo-hello'   // Jenkinsfile ve pipeline job ma seed job se build
for(i = 0; i < number_of_jobs; i++) {
    def name = list_of_parameters[i]
    pipelineJob('nguyen-job ' + i) {
        definition {
        parameters {
            stringParam('parameters', name, "parameter ma job se nhan") 
        }
            cpsScm {
                scm {
                    git {
                        branch(branch_of_git)  
                        remote {
                            url(url_of_git)
                            credentials(credential)
                        }
                    }
                }
                scriptPath(path)
            }
        }
    }
}
